<?php

use App\Http\Controllers\AdminController;
use App\Http\Controllers\CustomerController;
use App\Http\Controllers\DriverController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

// customer api
Route::post('register', [CustomerController::class, 'register']);
Route::post('login', [CustomerController::class, 'login']);
Route::post('logout', [CustomerController::class, 'logout']);
Route::get('getProfile/{uuid}', [CustomerController::class, 'getProfile']);
Route::post('updateProfile/{uuid}', [CustomerController::class, 'updateProfile']);
Route::post('addAddress/{uuid}', [CustomerController::class, 'addAddress']);
Route::get('getAddressList/{uuid}', [CustomerController::class, 'getAddressList']);
Route::post('resetPassword/{uuid}', [CustomerController::class, 'resetPassword']);
Route::post('changePassword/{uuid}', [CustomerController::class, 'changePassword']);
Route::post('forgotPassword', [CustomerController::class, 'forgotPassword']);
Route::get('getProductDetail/{uuid}', [CustomerController::class, 'getProductDetail']);
Route::get('getProductListing', [CustomerController::class, 'getProductListing']);
Route::get('getBrandListing', [CustomerController::class, 'getBrandListing']);
Route::get('getCategoryTypeListing', [CustomerController::class, 'getCategoryTypeListing']);
Route::get('getCategoryListing', [CustomerController::class, 'getCategoryListing']);
Route::get('getSubCategoryListing', [CustomerController::class, 'getSubCategoryListing']);
Route::post('addToCart', [CustomerController::class, 'addToCart']);
Route::get('getCartDetails', [CustomerController::class, 'getCartDetails']);
Route::delete('removeFromCart/{id}', [CustomerController::class, 'removeFromCart']);
Route::post('addToWishlist', [CustomerController::class, 'addToWishlist']);
Route::get('getWishlistData', [CustomerController::class, 'getWishlistData']);
Route::delete('removeFromWishlist/{id}', [CustomerController::class, 'removeFromWishlist']);
Route::post('addReview', [CustomerController::class, 'addReview']);
Route::get('getReviewList/{productId}', [CustomerController::class, 'getReviewList']);
Route::get('getMyReviewList', [CustomerController::class, 'getMyReviewList']);
Route::get('getFaqListing', [CustomerController::class, 'getFaqListing']);
Route::get('getAllOrder', [CustomerController::class, 'getAllOrder']);
Route::get('getOrderDetail/{uuid}', [CustomerController::class, 'getOrderDetail']);
// Route::get('getSizes', [CustomerController::class, 'getSizes']);


// admin api
Route::post('adminRegister', [AdminController::class, 'adminRegister']);
Route::post('adminLogin', [AdminController::class, 'adminLogin']);
Route::post('adminLogout', [AdminController::class, 'adminLogout']);
Route::get('getAdminProfile/{uuid}', [AdminController::class, 'getAdminProfile']);
Route::post('updateAdminProfile/{uuid}', [AdminController::class, 'updateAdminProfile']);
Route::get('getCustomerListing', [AdminController::class, 'getCustomerListing']);
Route::get('getCustomerDetail/{uuid}', [AdminController::class, 'getCustomerDetail']);
Route::get('getAllProducts', [AdminController::class, 'getAllProducts']);
Route::get('getProductDetail/{uuid}', [AdminController::class, 'getProductDetail']);
Route::post('addProduct', [AdminController::class, 'addProduct']);
Route::post('updateProduct/{uuid}', [AdminController::class, 'updateProduct']);
Route::delete('deleteProduct/{productId}', [AdminController::class, 'deleteProduct']);
Route::get('getCompletedOrders/{orderId}', [AdminController::class, 'getCompletedOrders']);
Route::get('getCancelledOrders/{orderId}', [AdminController::class, 'getCancelledOrders']);
Route::get('getOrdersListing', [AdminController::class, 'getOrdersListing']);
Route::get('getAllOrderDetail/{orderId}', [AdminController::class, 'getAllOrderDetail']);


/* Driver */
Route::post('updatePassword/{driverId}', [DriverController::class, 'updatePassword']);
Route::get('getEarningHistory/{driverId}', [DriverController::class, 'getEarningHistory']);
Route::get('getDriverWallet/{driverId}', [DriverController::class, 'getDriverWallet']);
