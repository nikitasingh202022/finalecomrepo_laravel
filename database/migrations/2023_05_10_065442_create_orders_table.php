<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_orders', function (Blueprint $table) {
            $table->integer('orderId')->autoIncrement();
            $table->uuid('uuid', 150)->default(DB::raw('(UUID())'))->unique()->nullable();
            $table->string('orderNo');
            $table->string('orderCode');
            $table->integer('customerId',false);
            $table->string('couponCode');
            $table->longText('orderAddress');
            $table->double('totalAmt',10,2);
            $table->double('discountedAmt',10,2);
            $table->enum('paymentMode',['online','cash']);
            $table->string('transId');
            $table->enum('paymentStatus',['success','pending','failed']);
            $table->enum('orderStatus',['pending','shipped','picked','delivered','cancelled']);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('customerId')->references('customerId')->on('tbl_customers')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
};
