<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_sizes', function (Blueprint $table) {
            $table->integer('sizeId')->autoIncrement();
            $table->integer('brandId',false);
            $table->enum('gender',['male','female','unisex']);
            $table->string('measurements');
            $table->boolean('isActive')->default(1)->comment('1 For Active 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('brandId')->references('brandId')->on('tbl_brands')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sizes');
    }
};
