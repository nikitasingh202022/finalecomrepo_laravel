<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_driver', function (Blueprint $table) {
            $table->integer('driverId')->autoIncrement();
            $table->string('uuid');
            $table->integer('costumerId');
            $table->integer('orderId');
            $table->string('driver_name');
            $table->string('driver_email');
            $table->string('driver_password');
            $table->integer('rating');
            $table->string('review');
            $table->string('jobId');
            $table->string('amount');
            $table->string('wallet');
            $table->string('earning');
            $table->string('total');
            $table->string('date');
            $table->string('remark');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_driver');
    }
};
