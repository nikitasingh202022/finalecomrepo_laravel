<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_wishlist', function (Blueprint $table) {
            $table->integer('wishlistId')->autoIncrement();
            $table->uuid('uuid', 150)->default(DB::raw('(UUID())'))->unique()->nullable();
            $table->integer('customerId',false);
            $table->integer('productId',false);
            $table->integer('catTypeId',false);
            $table->integer('catId',false);
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('customerId')->references('customerId')->on('tbl_customers')->onDelete('cascade');
            $table->foreign('productId')->references('productId')->on('tbl_products')->onDelete('cascade');
            $table->foreign('catTypeId')->references('catTypeId')->on('tbl_category_types')->onDelete('cascade');
            $table->foreign('catId')->references('catId')->on('tbl_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wishlists');
    }
};
