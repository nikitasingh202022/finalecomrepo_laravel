<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_addresses', function (Blueprint $table) {
            $table->integer('addressId')->autoIncrement();
            $table->integer('customerId',false);
            $table->enum('addressType',['Home','Work','Other']);
            $table->longText('address');
            $table->integer('cityId',false);
            $table->integer('stateId',false);
            $table->integer('countryId',false);
            $table->string('countryCode',50);
            $table->bigInteger('pinCode',false,10);
            $table->boolean('isDefault')->default(0)->comment('1 For Default 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('addresses');
    }
};
