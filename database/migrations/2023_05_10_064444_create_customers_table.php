<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_customers', function (Blueprint $table) {
            $table->integer('customerId')->autoIncrement();
            $table->uuid('uuid', 150)->default(DB::raw('(UUID())'))->unique()->nullable();
            $table->string('contInfoId',150)->nullable();
            $table->string('fullName');
            $table->enum('gender',['male','female','other']);
            $table->date('dob');
            $table->string('email');
            $table->string('password');
            $table->boolean('isEmailVerified')->nullable()->comment('1 For Verified 0 For Not');
            $table->string('mobile');
            $table->boolean('isMobileVerified')->nullable()->comment('1 For Verified 0 For Not');
            $table->bigInteger('otp',false)->nullable();
            $table->string('profilePicture');
            $table->longText('address');
            $table->integer('cityId',false);
            $table->integer('stateId',false);
            $table->integer('countryId',false);
            $table->string('countryCode',50);
            $table->enum('deviceType',['android','iphone','windows','other']);
            $table->double('walletAmount',10,2)->nullable();
            $table->rememberToken();
            $table->boolean('isActive')->default(1)->comment('1 For Active 0 For Not');
            $table->boolean('isDeleted')->default(0)->comment('1 For Deleted 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deletedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('customers');
    }
};
