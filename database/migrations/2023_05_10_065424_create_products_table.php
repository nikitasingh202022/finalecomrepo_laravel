<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_products', function (Blueprint $table) {
            $table->integer('productId')->autoIncrement();
            $table->uuid('uuid', 150)->default(DB::raw('(UUID())'))->unique()->nullable();
            $table->integer('catTypeId',false);
            $table->integer('catId',false);
            $table->integer('subCatId',false);
            $table->integer('brandId',false);
            $table->string('productName');
            $table->string('productImageId');
            $table->longText('productDesc');
            $table->integer('productQty',false);
            $table->string('unit');
            $table->double('productMrp',10,2)->comment('maximum retail price');
            $table->double('productMsp',10,2)->comment('minimum selling price');
            $table->date('productMfd')->nullable();
            $table->date('productExp')->nullable();
            $table->boolean('isActive')->default(1)->comment('1 For Active 0 For Not');
            $table->boolean('isDeleted')->default(0)->comment('1 For Deleted 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deletedAt')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('catTypeId')->references('catTypeId')->on('tbl_category_types')->onDelete('cascade');$table->foreign('catId')->references('catId')->on('tbl_categories')->onDelete('cascade');$table->foreign('brandId')->references('brandId')->on('tbl_brands')->onDelete('cascade');
            $table->foreign('subCatId')->references('catId')->on('tbl_categories')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
};
