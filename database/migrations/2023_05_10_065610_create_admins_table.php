<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_admins', function (Blueprint $table) {
            $table->integer('adminId')->autoIncrement();
            $table->tinyInteger('adminRole',false);
            $table->string('adminName');
            $table->string('adminEmail');
            $table->string('adminPassword');
            $table->string('adminMobile');
            $table->date('dob');
            $table->string('adminProfilePicture');
            $table->longText('adminAddress');
            $table->rememberToken();
            $table->boolean('isVerified')->default(0)->comment('1 For Active 0 For Not');
            $table->boolean('isActive')->default(1)->comment('1 For Active 0 For Not');
            $table->boolean('isDeleted')->default(0)->comment('1 For Deleted 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deletedAt')->default(DB::raw('CURRENT_TIMESTAMP'));
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('admins');
    }
};
