<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_categories', function (Blueprint $table) {
            $table->integer('catId')->autoIncrement();
            $table->uuid('uuid', 150)->default(DB::raw('(UUID())'))->unique()->nullable();
            $table->integer('catTypeId',false);
            $table->tinyInteger('parentId',false)->default(0)->comment('0 For Parent');
            $table->string('catName');
            $table->string('catImg');
            $table->boolean('isActive')->default(1)->comment('1 For Active 0 For Not');
            $table->boolean('isDeleted')->default(0)->comment('1 For Deleted 0 For Not');
            $table->timestamp('createdAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updateAt')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('deletedAt')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('catTypeId')->references('catTypeId')->on('tbl_category_types')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('categories');
    }
};
