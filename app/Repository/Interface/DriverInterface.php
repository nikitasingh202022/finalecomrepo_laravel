<?php

namespace App\Repository\Interface;


interface DriverInterface
{
    public function getEarningHistory($data);

    public function getDriverWallet($data);

    public function updatePassword($driverId, array $request);
}
