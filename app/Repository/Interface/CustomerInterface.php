<?php

namespace App\Repository\Interface;


interface CustomerInterface
{

    public function customerRegistration(array $data);

    public function customerLogin(array $data);

    public function customerLogout();

    public function getCustomerProfile($uuid);

    public function updateCustomerProfile($uuid, array $data);

    public function addAddress($uuid, array $data);

    public function getAddressList($uuid);

    public function getProductListing($filter,$key='');

    public function getProductDetail($uuid);

    public function getBrandListing($key='');

    public function getCategoryListing($key='');

    public function getCategoryTypeListing($key='');

    public function getSubCategoryListing($categoryId,$key='');

    public function addToCart(array $data);

    public function getCartDetails($key="");

    public function removeFromCart($id);

    public function addToWishlist(array $data);

    public function getWishlistData($filter,$key="");

    public function removeFromWishlist($id);

    public function getCustomerFromUuid($uuid);

    public function getCustomerFromToken();

    public function addReview(array $data);

    public function getReviewList($productId,$key="");

    public function getMyReviewList($key="");

    public function getFaqListing($key="");

    public function getAllOrder($key="");

    public function getOrderDetail($uuid);

    public function resetCustomerPassword($password);

    public function changeCustomerPassword($oldPassword, $password);





    public function getSizes($key='');

    public function forgotPassword($email, $password);

    public function getAccountDetail($uuid);

    public function addAccountDetail($uuid, array $data);

















}
