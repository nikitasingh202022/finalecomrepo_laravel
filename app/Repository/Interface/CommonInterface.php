<?php

namespace App\Repository\Interface;

interface CommonInterface
{

    public function generateOtp($digits,$expTime);

    public function verifyOtp($table,$key,$id,$otp);

    public function checkEmail($table,$email);

    public function checkMobile($table,$mobile);

    public function verifyEmailSendOtp($table,$email);

    public function verifyMobileSendOtp($table,$email);

    public function insertImage($image,$fileName,$filePath);

    public function insertBulkFiles($files,$fileName,$filePath);

    public function changePassword($table, $id, array $request);

    public function resetPassword($table, $id, array $request);

    public function insertData($table, array $data);

    public function insertDataWithUuid($table, array $data);

    public function getAllData($table, $key = "");

    public function getDataWhere($table, array $select, array $where);

    public function getAllDataWhere($table, array $where, $key = "", $orderBy = "desc");

    public function getAllDataWhereLimit($table, array $cond, $limit);

    public function getAllDataWhereWithLimitOffset($table, array $select, array $where, $key = "", $limit = 10, $offset = 0);

    public function getAllDataWhereWithOrderBy($table, array $select, array $where, $key = "", $orderBy = 'desc');

    public function updateData($table, array $cond, array $data);

    public function upload_image($image_data, $num, $path1);

    public function deleteDataPermanently($table, $id, $key);

    public function deleteData($table, array $where, $key);

    public function countData($table,array $cond, $select);

    public function changeStatus($table, $column, $id);







    // public function deleteDataWithImg($table, $request);

    // public function upload_image_with_compress($imageData, $path, $quality, $width, $height);

    // public function upload_compress_image($image_data, $path, $quality, $num);

    // public function send_mail($sender, $receiver, $mail_message, $mail_subject);

    // public function generateRandomString();

    // public function uniqueCouponCode($couponCode, $couponId = 0);

    // public function registration($table, array $data);

    // public function login($table, string $tokenName, array $data);

    // public function logout($table, $request);

}
