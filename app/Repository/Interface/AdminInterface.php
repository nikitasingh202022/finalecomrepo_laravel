<?php

namespace App\Repository\Interface;


interface AdminInterface
{
    public function adminRegister(array $data);

    public function adminLogin(array $data);

    public function adminLogout();

    public function getAdminProfile($uuid);

    public function updateAdminProfile($uuid, array $data);

    public function getCustomerListing($key = "");

    public function getCustomerDetail($uuid);

    public function getAllProducts($key = '');

    public function getProductDetail($uuid);

    public function addProduct(array $data, array $fileArray);

    public function updateProduct($id, array $data, array $fileArray);

    public function deleteProduct($id);




    public function addBrand(array $data);

    public function getBrandListing($key = '');

    public function deleteBrand($id);

    public function addCategory(array $data);

    public function getCategoryListing($key = '');

    public function deleteCategory($id);

    public function addCategoriesType(array $data);

    public function getCategoriesTypeListing($key = '');

    public function deleteCategoriesType($id);

    public function addSubCategory(array $data);

    public function getSubCategoryListing($key = '');

    public function deleteSubCategory($id);

    public function addFaq($table, array $data);

    public function getFaqListing($table, $key = "");






    public function getUpcomingDeliveries($uuid, array $data);

    public function getOrdersListing($key = "");

    public function getOrderDetail($orderId);

    public function getItemsSold($uuid, array $data);

    public function getCancelledOrders();

    public function getCompletedOrders();

    

}
