<?php

namespace App\Repository\Repository;

use App\Repository\Interface\CommonInterface;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;

class CommonRepository implements CommonInterface
{

    public function generateOtp($digits,$expTime)
    {
        $now = now();
        $characters = '0123456789';
        $string = '';

        for ($i = 0; $i < $digits; $i++) {
            $string .= $characters[mt_rand(0, strlen($characters) - 1)];
        }
        $otp= [
            'otp' => $string,
            'expire_at' => $now->addMinutes($expTime),
        ];
        return response([
            'message' => 'Success',
            'data' => $otp,
        ],200);
    }

    public function verifyOtp($table,$key,$id,$otp)
    {
        if($table!="" && $key!="" && $id!="" && $otp!=""){
            $check = DB::table($table)->where($key,$id)->first();
            if($check){
                if($check->otp==$otp){
                    return response()->json([
                        'status' => 'success',
                        'message' => 'Otp matched',
                    ], 200);
                }
                    return response()->json([
                        'status' => 'error',
                        'message' => 'Otp did not matched',
                    ], 200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'User not found for given id',
                ], 404);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'All fields required',
            ], 401);
        }
    }

    public function checkEmail($table,$email)
    {
        if($email!="" && $email!= null && $table!="" && $table!= null){
            if(preg_match("/^([a-z0-9\+_\-]+)(\.[a-z0-9\+_\-]+)*@([a-z0-9\-]+\.)+[a-z]{2,8}$/",$email)){
                $query = DB::table($table)->where('email',$email)->first();
                if($query){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please enter a valid email!',
                ], 422);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'All fields required!',
            ], 401);
        }

    }

    public function checkMobile($table,$mobile)
    {
        if($mobile!="" && $mobile!= null && $table!="" && $table!= null){
            if(preg_match("/^([+]\d{2})?\d{10}$/",$mobile)){
                $query = DB::table($table)->where('mobile',$mobile)->first();
                if($query){
                    return 1;
                }else{
                    return 0;
                }
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Please enter a valid mobile no!',
                ], 422);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Mobile no. and table field cannot be empty!',
            ], 401);
        }

    }

    public function verifyEmailSendOtp($table,$email)
    {
        $checkEmail = $this->checkEmail($table,$email);
        $status = json_decode($checkEmail);
        $now = now();
        if($status!=null && ($status==1||$status==0)){
            $otp= [
                'otp' => rand(123456, 999999),
                'expire_at' => $now->addMinutes(10),
            ];
            DB::table($table)->where('email',$email)->update($otp);
            //send otp on email
            return response()->json([
                'status' => 'success',
                'message' => 'OTP has been sent on your email.!',
            ], 200);
        }else{
            return $checkEmail;
        }
    }

    public function verifyMobileSendOtp($table,$mobile)
    {
        $checkMobile = $this->checkMobile($table,$mobile);
        $status = json_decode($checkMobile);
        $now = now();
        if($status!=null && ($status==1||$status==0)){
            $otp= [
                'otp' => rand(123456, 999999),
                'expire_at' => $now->addMinutes(10)
            ];
            DB::table($table)->where('mobile',$mobile)->update($otp);
            //send otp on mobile
            return response()->json([
                'status' => 'success',
                'message' => 'OTP has been sent on Your Mobile Number.!',
            ], 200);
        }else{
            return $checkMobile;
        }
    }

    public function insertImage($image,$fileName,$filePath)
    {
        if(isset($image) && $image!="")
        {
            $fileName = $fileName . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . $filePath, $fileName);
            return $fileName;
        }else{
            return 0;
        }
    }

    public function insertBulkFiles($files,$fileName,$filePath)
    {
        $fileArray = [];
        if(isset($files) && $files!="")
        {
            foreach($files as $file)
            {
                $newFileName = $fileName . rand(1234,9999) . '.' . $file->getClientOriginalExtension();
                $path = $file->move(public_path() . $filePath, $newFileName);
                array_push($fileArray,$newFileName);
            }
            // $fileArray = implode(",",$fileArray);
            return json_encode($fileArray);
        }else{
            return 0;
        }


    }

    public function changePassword($table, $id, array $request)
    {
        $user = DB::table($table)->where('id', $id)->first();

        if(!$user || !Hash::check($request['oldPassword'], $user->password)){
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            // 'password' => bcrypt($request['password']),
            'password' => Hash::make($request['password']),
        ];

        $result = DB::table($table)->find($id)->update($data);
        if($result){
            return response([
                'message' => 'Success',
            ],201);
        }
    }

    public function resetPassword($table, $id, array $request)
    {
        $user = DB::table($table)->where('id', $id)->first();

        if(!$user){
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            'password' => bcrypt($request['password']),
        ];

        $result = DB::table($table)->find($id)->update($data);
        if($result){
            return response([
                'message' => 'Success',
            ],201);
        }
    }

    public function insertData($table, array $data)
    {
        $query = DB::table($table)->insertGetId($data);
        if($query){
            return response([
                'message' => 'Success',
                'data' => $query,
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function insertDataWithUuid($table, array $data)
    {
        $data['uuid'] = (string) str::uuid();
        $query = DB::table($table)->insertGetId($data);
        if($query){
            return response([
                'message' => 'Success',
                'data' => $query,
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

	public function getAllData($table, $key = "")
	{
        if (isset($key)&&!empty($key)){
			$query = DB::table($table)->orderBy($key,"desc")->get();
		}else{
            $query = DB::table($table)->get();
        }

		if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
        }
	}

	public function getDataWhere($table, array $select, array $where)
	{
        if(!sizeof($select)){
            $select = '*';
        }
        $query = DB::table($table)->select($select)->where($where)->get();

		if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
		}
	}

	public function getAllDataWhere($table, array $where, $key = "", $orderBy = "desc")
	{

        $query = DB::table($table)->where($where);

		if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, $orderBy)->get();
		}else{
            $query = $query->get();
        }

		if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
		}
	}

    public function getAllDataWhereLimit($table, array $cond, $limit)
    {
        $query = DB::table($table)->where($cond)->take($limit)->get();

        if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
		}
    }

    public function getAllDataWhereWithLimitOffset($table, array $select, array $where, $key = "", $limit = 10, $offset = 0)
    {
        if(!sizeof($select)){
            $select = '*';
        }
        $query = DB::table($table)->select($select)->where($where)->skip($offset)->take($limit);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, 'desc')->get();
		}else{
            $query = $query->get();
        }

        if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
		}
    }

    public function getAllDataWhereWithOrderBy($table, array $select, array $where, $key = "", $orderBy = 'desc')
    {
        if(!sizeof($select)){
            $select = '*';
        }

        $query = DB::table($table)->select($select)->where($where);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, $orderBy)->get();
		}else{
            $query = $query->get();
        }

        if ($query->count() > 0){
            return response([
                'message' => 'Success',
                'data' => $query->toArray(),
            ],200);
		}else{
			return response([
                'message' => 'No data found',
                'data' => [],
            ],200);
		}
    }

    public function updateData($table, array $cond, array $data)
	{
        $query = DB::table($table)->where($cond)->update($data);

		if ($query){
            return response([
                'status' => 'success',
                'message' => 'Success',
            ],201);
		}else{
			return response([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
		}
	}

    public function upload_image($image_data, $num, $path1)
	{
        $image = md5(date("d-m-y:h:i s")) . "_" . $num;
        if (is_array($image_data)) {
            $fileName = $image_data['image']->getClientOriginalName();
            $extension = $image_data['image']->getClientOriginalExtension();
            if($image_data['image']->move(public_path() . $path1, $image . '.' . $extension)){
                $image = $image . '.' . $extension;
            }else {
				$image = Null;
			}
            return $image;
        }else{
            return 0;
        }

	}

    public function deleteDataPermanently($table, $id, $key)
    {
        $query = DB::table($table)->where($key,$id)->delete();
		if ($query){
            return response([
                'status' => 'success',
                'message' => 'Success',
            ],200);
		}else{
			return response([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
		}
    }

    public function deleteData($table, array $where, $key)
    {
        $query = DB::table($table)->where($where)->update([$key => 1]);
		if ($query){
            return response([
                'status' => 'success',
                'message' => 'Success',
            ],200);
		}else{
			return response([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
		}
    }

    public function countData($table, array $cond, $select)
    {
        if($select==""){
            $select="*";
        }
        $query = DB::table($table)->select($select)->where($cond)->count();
        return $query;
    }

    public function changeStatus($table, $column, $uuid)
    {
        $status = DB::table($table)->where('uuid',$uuid)->value($column);

        if($status == 1){
            $query = DB::table($table)->where('uuid',$uuid)->update(array($column => 0));
        }else{
            $query = DB::table($table)->where('uuid',$uuid)->update(array($column => 1));
        }
        return $query;
    }





































    public function upload_image_with_compress($imageData, $path, $quality, $width, $height)
    {


    }

    public function upload_compress_image($image_data, $path, $quality, $num)
    {

    }



}
