<?php

namespace App\Repository\Repository;

use App\Models\Address;
use App\Models\Brand;
use App\Models\Cart;
use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Customer;
use App\Models\Faq;
use App\Models\Order;
use App\Models\Product;
use App\Models\Review;
use App\Repository\Interface\CustomerInterface;
use Illuminate\Support\Facades\DB;
use App\Models\Wishlist;
use App\Repository\Interface\CommonInterface;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Str;
use Tymon\JWTAuth\Facades\JWTAuth;

class CustomerRepository implements CustomerInterface
{
    public $common;
    public function __construct(CommonInterface $common)
    {
        $this->common = $common;
    }

    public function customerRegistration(array $data)
    {
        $query = Customer::create($data);

        if($query){
            $token = Auth::login($query);
            return response()->json([
                'status' => 'success',
                'message' => 'Customer created successfully!',
                'customer' => $query,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function customerLogin(array $data)
    {
        if(isset($data['email'])){
            $query = Customer::select('uuid','customerId')->where('email',$data['email'])->first();
        }else if(isset($data['mobile'])){
            $query = Customer::select('uuid','customerId')->where('mobile',$data['mobile'])->first();
        }
        if(!$query && $query==null){
            return response()->json([
                'status' => 'error',
                'message' => 'Mobile or Email not registered!',
            ], 401);
        }
        $customClaims = [
            'customerUuid' => $query['uuid'],
            'customerId' => $query['customerId'],
        ];
        $token = Auth::claims($customClaims)->attempt($data);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized!',
            ], 401);
        }
        $user = Auth::user();
        return response()->json([
                'status' => 'success',
                'user' => $user,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
        ]);
    }

    public function customerLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);
    }

    public function getCustomerProfile($uuid)
    {
        // from token
        // $token = JWTAuth::getToken();
        // $token = JWTAuth::decode($token);
        // $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $token['customerUuid'])->first();

        // from auth
        // $customer = Auth::user();

        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $uuid)->first();

        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function updateCustomerProfile($uuid, array $data)
    {
        $query = Customer::where('uuid',$uuid)->update($data);

        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Customer updated successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function addAddress($uuid, array $data)
    {
        $query = Customer::where('uuid',$uuid)->first();

        if($query){
            $data['customerId'] = $query['customerId'];
            $result = Address::create($data);
            if($result){
                return response()->json([
                    'status' => 'success',
                    'message' => 'Address added successfully!',
                ],201);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found!',
            ],404);
        }

    }

    public function getAddressList($uuid)
    {
        $customerId = Customer::select('customerId')->where('uuid', $uuid)->first();
        if($customerId){
            $query = Address::select('addressId','pinCode','address','city','state','country','countryCode','isDefault')->where('customerId', $customerId['customerId'])
                            ->leftJoin('tbl_cities', 'tbl_cities.cityId', 'tbl_addresses.cityId')
                            ->leftJoin('tbl_states', 'tbl_states.stateId', 'tbl_addresses.stateId')
                            ->leftJoin('tbl_countries', 'tbl_countries.countryId', 'tbl_addresses.countryId')
                            ->get();
            if($query){
                return response()->json([
                    'status' => 'success',
                    'customer' => $query,
                ],200);
            }else{
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ],401);
            }
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Customer not found',
            ],404);
        }
    }

    public function addToCart(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId'],'size'=>$data['size']];
        $check = Cart::select('cartId','cartQty')->where($con)->first();
        if(!$check){
            $query = Cart::insertGetId($data);
        }else{
            $query = Cart::where('cartId',$check['cartId'])->update(['cartQty'=>$check['cartQty']+1]);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product added to cart successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCartDetails($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $cond = [
            'tbl_carts.customerId' => $customerId,
        ];
        $query = Cart::select('tbl_carts.cartId','tbl_carts.uuid AS cartUuid','tbl_carts.cartQty','tbl_carts.size','tbl_carts.isChecked','tbl_products.productId','tbl_products.uuid AS productUuid','tbl_products.productDesc','tbl_products.productMrp','tbl_product_images.productImage')
                        ->leftJoin('tbl_products','tbl_products.productId' ,'tbl_carts.productId')
                        ->leftJoin('tbl_product_images','tbl_products.productId' ,'tbl_product_images.productId')
                        ->where($cond);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
                ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'cartData' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function removeFromCart($id)
    {
        $query = Cart::where('cartId', $id)->delete();
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product removed from cart successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getAllOrder($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $query = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus')
                        ->where('customerId',$customerId);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'orders' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getOrderDetail($uuid)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $con=['tbl_orders.uuid' => $uuid, 'customerId' => $customerId];
        $query = Order::select('tbl_orders.orderId','tbl_orders.uuid AS orderUuid','tbl_orders.orderCode','tbl_orders.couponCode','tbl_orders.orderAddress','tbl_orders.totalAmt','tbl_orders.discountedAmt','tbl_orders.paymentMode','tbl_orders.transId','tbl_orders.paymentStatus','tbl_orders.orderStatus')
                        ->where($con)->first();
        if($query){
            return response()->json([
                'status' => 'success',
                'order' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getProductListing($filter,$key='')
    {
        $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
        ];
        $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', function($join){
                            $join->on('tbl_products.catId', 'cat.catId')
                            ->where('cat.parentId', 1 );
                        })
                        ->leftJoin('tbl_categories AS sub_cat', function($join){
                            $join->on('tbl_products.subCatId', 'sub_cat.catId')
                            ->where('sub_cat.parentId', 0 );
                        })
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->where($con);

        // if(isset($filter) && $filter!=""){
        //     $filters = preg_split ("/\,/", $filter);
        //     $query = $query->whereIn('tbl_wishlist.category',$filters)
        //                     ->orWhereIn('tbl_wishlist.categoryType',$filters);
        // }

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }

        if($query){
            return response()->json([
                'status' => 'success',
                'productList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getProductDetail($uuid)
    {
        $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
            'tbl_products.uuid' => $uuid,
        ];
        $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', function($join){
                            $join->on('tbl_products.catId', 'cat.catId')
                            ->where('cat.parentId', 1 );
                        })
                        ->leftJoin('tbl_categories AS sub_cat', function($join){
                            $join->on('tbl_products.subCatId', 'sub_cat.catId')
                            ->where('sub_cat.parentId', 0 );
                        })
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->where($con)->first();

        if($query){
            return response()->json([
                'status' => 'success',
                'product' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getBrandListing($key='')
    {
        $con = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = Brand::select('brandId','uuid','brandName','brandImg',)
                        ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }

        if($query){
            return response()->json([
                'status' => 'success',
                'brandList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCategoryListing($key='')
    {

        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 1,
        ];
        $query = Category::select('tbl_categories.catId','tbl_categories.uuid','tbl_categories.catName','tbl_categories.catImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
                        ->where($con);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'categoryList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCategoryTypeListing($key='')
    {
        $con = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = CategoryType::select('catTypeId','uuid','catTypeName','catTypeImg',)
                        ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'categoryTypeList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getSubCategoryListing($categoryId,$key='')
    {
        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 0,
            'tbl_categories.parentCatId' => $categoryId,
        ];
        $query = Category::select('tbl_categories.catId AS subCatId','tbl_categories.uuid AS subCatUuid','tbl_categories.catName AS subCatName','tbl_categories.catImg AS subCatImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
                        ->where($con);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'subCategoryList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function addReview(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId']];
        $check = Review::select('reviewId')->where($con)->first();
        if(!$check){
            $image=$data['reviewFile'];
            if(isset($image) && $image!="")
            {
                $data['reviewFile'] = $this->common->insertBulkFiles($image,'customerReview','/images/customerReview');
                // $fileName = "customerReview" . time() . '.' . $image->getClientOriginalExtension();
                // $path = $image->move(public_path() . '/images/customerReview', $fileName);
                // $data['reviewFile'] = $fileName;
            }
            $query = Review::insertGetId($data);
        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Review already exist!',
            ],200);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Review added successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getReviewList($productId,$key = "")
    {
       $query = Review::select('tbl_reviews.reviewId','tbl_reviews.uuid AS reviewUuid','tbl_reviews.rating', 'tbl_reviews.review', 'tbl_reviews.reviewFile AS images','tbl_customers.fullName', 'tbl_products.productName', 'tbl_reviews.createdAt')
                        ->leftJoin('tbl_customers', 'tbl_reviews.customerId', 'tbl_customers.customerId')
                        ->leftJoin('tbl_products', 'tbl_reviews.productId', 'tbl_products.productId')
                        ->where('tbl_reviews.productId',$productId);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'reviewList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No reviews available',
            ],404);
        }
    }

    public function getMyReviewList($key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data = ['tbl_reviews.customerId' => $token['customerId']];
        $query = Review::select('tbl_reviews.reviewId','tbl_reviews.uuid AS reviewUuid','tbl_reviews.rating', 'tbl_reviews.review', 'tbl_reviews.reviewFile AS images','tbl_customers.fullName', 'tbl_products.productName', 'tbl_reviews.createdAt')
                        ->leftJoin('tbl_customers', 'tbl_reviews.customerId', 'tbl_customers.customerId')
                        ->leftJoin('tbl_products', 'tbl_reviews.productId', 'tbl_products.productId')
                        ->where($data);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'reviewList' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No reviews available',
            ],404);
        }
    }

    public function getFaqListing($key = "")
    {
        $cond = [
            'isActive' => 1,
        ];
        $query = Faq::where($cond);
            if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'faqs' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'No faqs available',
            ],404);
        }
    }

    public function addToWishlist(array $data)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $data['customerId'] = $token['customerId'];
        $data['uuid'] = (string)Str::uuid();
        $con= ['customerId'=>$data['customerId'],'productId'=>$data['productId']];
        $check = Wishlist::select('wishlistId')->where($con)->first();
        if(!$check){
            $query = Wishlist::insertGetId($data);
        }else{
            return response()->json([
                'status' => 'success',
                'message' => 'Product already exist!',
            ],200);
        }
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product added to wishlist successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getWishlistData($filter,$key = "")
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];
        $cond = [
            'tbl_wishlist.customerId' => $customerId,
        ];

        $query = Wishlist::select('tbl_wishlist.wishlistId', 'tbl_wishlist.uuid AS wishlistUuid','tbl_wishlist.category','tbl_wishlist.categoryType','tbl_products.productId','tbl_products.uuid AS productUuid','tbl_products.productDesc','tbl_products.productMrp','tbl_product_images.productImage')
                            ->leftJoin('tbl_products','tbl_products.productId' ,'tbl_wishlist.productId')
                            ->leftJoin('tbl_product_images','tbl_products.productId' ,'tbl_product_images.productId')
                            ->where($cond);

        if(isset($filter) && $filter!=""){
            $filters = preg_split ("/\,/", $filter);
            $query = $query->whereIn('tbl_wishlist.category',$filters)
                            ->orWhereIn('tbl_wishlist.categoryType',$filters);
        }

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
            ->get();
        } else {
            $query = $query->get();
        }

        if($query){
            return response()->json([
                'status' => 'success',
                'wishlist' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function removeFromWishlist($id)
    {
        $query = Wishlist::where('wishlistId', $id)->delete();
        if($query){
            return response()->json([
                'status' => 'success',
                'message' => 'Product removed from wishlist successfully!',
            ],201);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCustomerFromUuid($uuid)
    {
        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $uuid)->first();
        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function getCustomerFromToken()
    {
        // from token
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $query = Customer::select('customerId','profilePicture','fullName','email','dob','mobile','walletAmount','deviceType')->where('uuid', $token['customerUuid'])->first();
        // return $query;
        // from auth
        // $customer = Auth::user();

        if($query){
            return response()->json([
                'status' => 'success',
                'customer' => $query,
            ],200);
        }else{
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ],401);
        }
    }

    public function resetCustomerPassword($password)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];

        $customer = Customer::where('customerId', $customerId)->first();

        if(!$customer){
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            'password' => Hash::make($password),
        ];

        $result = Customer::where('customerId', $customerId)->update($data);
        if($result){
            return response([
                'message' => 'Success',
            ],201);
        }else{
            return response([
                'status' => 'error',
                'message' => 'Something went wrong!'
            ], 401);
        }
    }

    public function changeCustomerPassword($oldPassword, $password)
    {
        $token = JWTAuth::getToken();
        $token = JWTAuth::decode($token);
        $customerId = $token['customerId'];

        $customer = Customer::where('customerId', $customerId)->first();

        if(!$customer || !Hash::check($oldPassword, $customer->password)){
            return response([
                'status' => 'error',
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            // 'password' => bcrypt($request['password']),
            'password' => Hash::make($password),
        ];

        $result = Customer::where('customerId', $customerId)->update($data);
        if($result){
            return response([
                'message' => 'Success',
            ],201);
        }else{
            return response([
                'status' => 'error',
                'message' => 'Something went wrong!'
            ], 401);
        }
    }








    public function forgotPassword($email, $password)
    {

    }

    public function getSizes($key='')
    {

    }

    public function getAccountDetail($uuid)
    {

    }

    public function addAccountDetail($uuid, array $data)
    {

    }


}
