<?php

namespace App\Repository\Repository;

use App\Models\Admin;
use App\Models\Brand;
use App\Models\Category;
use App\Models\CategoryType;
use App\Models\Customer;
use App\Models\Driver;
use App\Models\Driver_job;
use App\Models\Order;
use App\Models\Product;
use App\Models\ProductImage;
use App\Repository\Interface\AdminInterface;
use Carbon\Carbon;
use GuzzleHttp\Psr7\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Tymon\JWTAuth\Facades\JWTAuth;

class AdminRepository implements AdminInterface
{

    public function adminRegister(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = Admin::create($data);

        if ($query) {
            $token = Auth::login($query);
            return response()->json([
                'status' => 'success',
                'message' => 'Admin created successfully!',
                'admin' => $query,
                'authorization' => [
                    'token' => $token,
                    'type' => 'bearer',
                ]
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function adminLogin(array $data)
    {
        if (isset($data['adminEmail'])) {
            $query = Admin::select('uuid', 'adminId', 'password')->where('adminEmail', $data['adminEmail'])->first();
        } else if (isset($data['adminMobile'])) {
            $query = Admin::select('uuid', 'adminId', 'password')->where('adminMobile', $data['adminMobile'])->first();
        }
        if (!$query && $query == null) {
            return response()->json([
                'status' => 'error',
                'message' => 'Mobile or Email not registered!',
            ], 401);
        }
        $customClaims = [
            'adminUuid' => $query['uuid'],
            'adminId' => $query['adminId'],
        ];

        $token = Auth::claims($customClaims)->attempt($data);
        dd($token);
        if (!$token) {
            return response()->json([
                'status' => 'error',
                'message' => 'Unauthorized!',
            ], 401);
        }
        $user = Auth::user();
        return response()->json([
            'status' => 'success',
            'user' => $user,
            'authorization' => [
                'token' => $token,
                'type' => 'bearer',
            ]
        ]);
    }

    public function adminLogout()
    {
        Auth::logout();
        return response()->json([
            'status' => 'success',
            'message' => 'Successfully logged out!',
        ]);
    }

    public function getAdminProfile($uuid)
    {
        // from token
        // $token = JWTAuth::getToken();
        // $token = JWTAuth::decode($token);
        // $query = Admin::select('adminId','adminProfilePicture','adminName','adminEmail','dob','adminMobile')->where('uuid', $token['adminUuid'])->first();

        // from auth
        // $admin = Auth::user();

        $query = Admin::select('adminId', 'adminProfilePicture', 'adminName', 'adminEmail', 'dob', 'adminMobile')->where('uuid', $uuid)->first();

        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'adminData' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function updateAdminProfile($uuid, array $data)
    {
        if (!sizeof($data)) {
            return response()->json([
                'status' => 'error',
                'message' => 'Nothing to update!',
            ], 200);
        } else {
            $query = Admin::where('uuid', $uuid)->update($data);
            if ($query) {
                return response()->json([
                    'status' => 'success',
                    'message' => 'Admin updated successfully!',
                ], 201);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ], 401);
            }
        }
    }

    public function getCustomerListing($key = "")
    {
        $cond = [
            'isActive' => 1,
            'isDeleted' => 0
        ];
        $query = Customer::select('customerId', 'uuid AS customerUuid', 'profilePicture', 'fullName', 'email', 'dob', 'mobile', 'walletAmount', 'deviceType')->where($cond);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'customerList' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getCustomerDetail($uuid)
    {
        $query = Customer::select('customerId', 'profilePicture', 'fullName', 'email', 'dob', 'mobile', 'walletAmount', 'deviceType')->where('uuid', $uuid)->first();
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'customer' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getAllProducts($key = '')
    {
        $sql = "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImage
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories AS cat ON cat.catId=tbl_products.catId AND cat.parentId= 0
        LEFT JOIN tbl_categories AS sub_cat ON sub_cat.catId=tbl_products.subCatId AND cat.parentId= 1
        WHERE tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        // dd($sql);
        $query = DB::select(DB::raw($sql));
        /* $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
        ];
        $query = Product::select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
                        ->leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', function($join){
                            $join->on('tbl_products.catId', 'cat.catId')
                            ->where('cat.parentId', 1 );
                        })
                        ->leftJoin('tbl_categories AS sub_cat', function($join){
                            $join->on('tbl_products.subCatId', 'sub_cat.catId')
                            ->where('sub_cat.parentId', 0 );
                        })
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->where($con); */

        /* if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        } */

        if ($query) {
            if (count($query) > 0) {
                return response([
                    'status' => 'success',
                    'productList' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getProductDetail($uuid)
    {
        $sql = "SELECT tbl_products.productId,tbl_products.uuid AS productUuid,tbl_products.productName,tbl_products.productDesc,tbl_products.unit,tbl_products.productQty,tbl_products.productMrp,tbl_products.productMsp,tbl_products.productMfd,tbl_products.productExp,tbl_brands.brandName,tbl_brands.brandImg,cat.catName,cat.catImg,sub_cat.catName AS subCatName,sub_cat.catImg AS subCatImg,tbl_category_types.catTypeName,tbl_category_types.catTypeImg,GROUP_CONCAT((tbl_product_images.productImage)) AS productImage
        FROM tbl_products
        LEFT JOIN tbl_product_images ON tbl_product_images.productId=tbl_products.productId
        LEFT JOIN tbl_brands ON tbl_brands.brandId=tbl_products.brandId
        LEFT JOIN tbl_category_types ON tbl_category_types.catTypeId=tbl_products.catTypeId
        LEFT JOIN tbl_categories AS cat ON cat.catId=tbl_products.catId AND cat.parentId= 0
        LEFT JOIN tbl_categories AS sub_cat ON sub_cat.catId=tbl_products.subCatId AND cat.parentId= 1
        WHERE tbl_products.uuid='$uuid' AND tbl_products.isActive=1 AND tbl_products.isDeleted=0
        GROUP BY tbl_products.productId";
        // dd($sql);
        $query = DB::select(DB::raw($sql));
        /* $con = [
            'tbl_products.isActive' => 1,
            'tbl_products.isDeleted' => 0,
            'tbl_products.uuid' => $uuid,
        ];
        $query = Product::leftJoin('tbl_brands', 'tbl_products.brandId', 'tbl_brands.brandId')
                        ->leftJoin('tbl_categories AS cat', function($join){
                            $join->on('tbl_products.catId', 'cat.catId')
                            ->where('cat.parentId', 1 );
                        })
                        ->leftJoin('tbl_categories AS sub_cat', function($join){
                            $join->on('tbl_products.subCatId', 'sub_cat.catId')
                            ->where('sub_cat.parentId', 0 );
                        })
                        ->leftJoin('tbl_category_types', 'tbl_products.catTypeId', 'tbl_category_types.catTypeId')
                        ->leftJoin('tbl_product_images', 'tbl_products.productId', 'tbl_product_images.productId')
                        ->select('tbl_products.productId','tbl_products.uuid','tbl_products.productName','tbl_products.productDesc','tbl_products.unit','tbl_products.productQty','tbl_products.productMrp','tbl_products.productMsp','tbl_products.productMfd','tbl_products.productExp','tbl_product_images.productImage','tbl_brands.brandName','tbl_brands.brandImg','cat.catName','cat.catImg','sub_cat.catName AS subCatName','sub_cat.catImg AS subCatImg','tbl_category_types.catTypeName','tbl_category_types.catTypeImg')
                        ->where($con)->first(); */

        if ($query) {
            if (count($query) > 0) {
                return response([
                    'status' => 'success',
                    'product' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function addProduct(array $data, array $fileArray)
    {
        $data['uuid'] = (string)Str::uuid();
        $query = Product::insertGetId($data);
        if ($query) {
            if (sizeof($fileArray)) {
                foreach ($fileArray as $image) {
                    $imageData = [
                        'productId' => $query,
                        'productImage' => $image
                    ];
                    $query2 = ProductImage::insert($imageData);
                }
            }
            return response()->json([
                'status' => 'success',
                'message' => 'Product added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function updateProduct($uuid, array $data, array $fileArray)
    {
        if (!sizeof($data)) {
            return response()->json([
                'status' => 'success',
                'message' => 'Nothing to update!',
            ], 200);
        } else {
            $data['updateAt'] = Carbon::now()->toDateTimeString();
            $query = Product::where('uuid', $uuid)->update($data);
            if ($query) {
                if (sizeof($fileArray)) {
                    $productId = Product::where('uuid', $uuid)->first();
                    foreach ($fileArray as $image) {
                        $imageData = [
                            'productId' => $productId,
                            'productImage' => $image
                        ];
                        $query2 = ProductImage::insert($imageData);
                    }
                }
                return response()->json([
                    'status' => 'success',
                    'message' => 'Product updated successfully!',
                ], 201);
            } else {
                return response()->json([
                    'status' => 'error',
                    'message' => 'Something went wrong!',
                ], 401);
            }
        }
    }

    public function deleteProduct($productId)
    {
        $product = Product::where('productId', $productId)->first();
        if ($product) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query = Product::where('productId', $productId)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Product not found!',
            ], 404);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Product deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }









    public function addBrand(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $con = ['brandName' => $data['brandName'], 'brandName' => $data['brandName']];
        $check = Brand::select('brandName')->where($con)->first();
        if (!$check) {
            $query = Brand::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand already exist!',
            ], 200);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }


    public function getBrandListing($key = '')
    {
        $con = [
            'tbl_brands.isActive' => 1
        ];
        $query = Brand::select('tbl_brands.brandId', 'tbl_brands.uuid', 'tbl_brands.brandName', 'tbl_brands.brandImg',)
            ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'brandList' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function deleteBrand($brandId)
    {
        $brand = Brand::where('brandId', $brandId)->first();
        if ($brand) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query = Brand::where('brandId', $brandId)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Brand not found!',
            ], 404);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Brand deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getOrdersListing($key = "")
    {
        $query = Order::select('orderId', 'orderNo', 'orderCode', 'customerId', 'couponCode', 'orderAddress', 'orderAddress', 'totalAmt', 'discountedAmt', 'paymentMode', 'transId', 'paymentStatus', 'orderStatus');
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'orderList' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }


    public function getOrderDetail($orderId)
    {
        $query = Order::select('orderId', 'orderNo', 'orderCode', 'customerId', 'couponCode', 'orderAddress', 'totalAmt', 'discountedAmt', 'paymentMode', 'transId', 'paymentStatus', 'paymentStatus', 'orderStatus')->where('orderId', $orderId)->first();
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'customer' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }


    public function addCategory(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $con = ['catName' => $data['catName'], 'catName' => $data['catName']];
        $check = Category::select('catName')->where($con)->first();
        if (!$check) {
            $query = Category::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'Category  already exist!',
            ], 200);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Category added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }
    public function getCategoryListing($key = '')
    {

        $con = [
            'tbl_categories.isActive' => 1,
            'tbl_categories.isDeleted' => 0,
            'tbl_categories.parentId' => 0,
        ];
        $query = Category::select('tbl_categories.catId', 'tbl_categories.uuid', 'tbl_categories.catName', 'tbl_categories.catImg', 'tbl_category_types.catTypeName', 'tbl_category_types.catTypeImg')
            ->leftJoin('tbl_category_types', 'tbl_categories.catTypeId', 'tbl_category_types.catTypeId')
            ->where($con);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'categoryList' => $query,
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }


    public function deleteCategory($catId)
    {
        $product = Category::where('catId', $catId)->first();
        if ($product) {
            $data = [
                'deletedAt' => Carbon::now()->toDateTimeString(),
                'isDeleted' => 1
            ];
            $query = Category::where('catId', $catId)->update($data);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Category not found!',
            ], 404);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Category deleted successfully!',
            ], 200);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }


    public function addCategoriesType(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $con = ['catTypeName' => $data['catTypeName'], 'catTypeName' => $data['catTypeName']];
        $check = CategoryType::select('catTypeName')->where($con)->first();
        if (!$check) {
            $query = CategoryType::insertGetId($data);
        } else {
            return response()->json([
                'status' => 'success',
                'message' => 'CatTypeName already exist!',
            ], 200);
        }
        if ($query) {
            return response()->json([
                'status' => 'success',
                'message' => 'Category Type added successfully!',
            ], 201);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getCategoriesTypeListing($key = '')
    {
        $con = [
            'tbl_mst_category_types.catTypStatus' => 1
        ];
        $query = DB::table('tbl_mst_category_types')
            ->select('tbl_mst_category_types.catTypeId', 'tbl_mst_category_types.uuid', 'tbl_mst_category_types.catTypName', 'tbl_mst_category_types.catTypImg',)
            ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        return $query;
    }

    public function deleteCategoriesType($id)
    {
        $query = CategoryType::where('catTypeId', $id)->delete();
        return $query;
    }

    public function addSubCategory(array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $result = Category::insertGetId($data);
        return $result;
    }

    public function getSubCategoryListing($key = '')
    {
        $con = [
            'tbl_mst_subcategories.subCatStatus' => 1
        ];
        $query = DB::table('tbl_mst_subcategories')
            ->select('tbl_mst_subcategories.subCatId', 'tbl_mst_subcategories.uuid', 'tbl_mst_subcategories.subCatName', 'tbl_mst_subcategories.subCatImg', 'tbl_mst_category_types.catTypName', 'tbl_mst_category_types.catTypImg')
            ->leftJoin('tbl_mst_category_types', 'tbl_mst_subcategories.catTypeId', 'tbl_mst_category_types.catTypeId')
            ->leftJoin('tbl_mst_categories', 'tbl_mst_subcategories.catId', 'tbl_mst_categories.catId')
            ->where($con);

        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")->get();
        } else {
            $query = $query->get();
        }
        return $query;
    }

    public function deleteSubCategory($id)
    {
        $query = Category::where('subCatId', $id)->delete();
        return $query;
    }

    public function addFaq($table, array $data)
    {
        $data['uuid'] = (string)Str::uuid();
        $result = DB::table($table)->insertGetId($data);
        return $result;
    }

    public function getFaqListing($table, $key = "")
    {
        $cond = [

            'tbl_faq.faqStatus' => 1,
        ];
        $query = DB::table($table)->where($cond);
        if (isset($key) && !empty($key)) {
            $query = $query->orderBy($key, "desc")
                ->get();
        } else {
            $query = $query->get();
        }
        return $query;
    }

    public function getItemsSold($uuid, array $data)
    {
    }

    public function getCancelledOrders()
    {
        $con = [
            'orderStatus' => 'Cancelled',
        ];
        $query = Order::where($con)->get();
        if ($query) {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getCompletedOrders()
    {
        $con = [
            'orderStatus' => 'Complete',
        ];
        $query = Order::where($con)->get();
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'success',
                    'orderList' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function getUpcomingDeliveries($uuid, array $data)
    {
    }
}
