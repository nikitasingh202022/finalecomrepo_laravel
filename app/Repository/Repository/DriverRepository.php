<?php

namespace App\Repository\Repository;

use App\Models\Driver;
use App\Models\Driver_job;
use App\Repository\Interface\DriverInterface;
use Illuminate\Support\Facades\Hash;

class DriverRepository implements DriverInterface
{
    public function getEarningHistory($driverId)
    {
        $con = ['driverId' => $driverId];
        $query = Driver_job::select('tbl_driver_jobs.jobId', 'tbl_driver_jobs.earning', 'tbl_driver_jobs.jobDate', 'tbl_driver_jobs.remarks', 'tbl_orders.orderNo', 'tbl_orders.orderStatus')
            ->leftJoin('tbl_orders', 'tbl_orders.orderId', 'tbl_driver_jobs.orderId')->where($con)->get();
        if ($query) {
            if (count($query) > 0) {
                return response([
                    'status' => 'success',
                    'EarningHistory' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'No data found',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }
    public function getDriverWallet($driverId)
    {
        $con = [
            'driverId' => $driverId
        ];
        $query = Driver::select('walletAmount', 'total_earnings')->where($con)->first();
        if ($query) {
            if ($query->count() > 0) {
                return response([
                    'status' => 'Success',
                    'driverWallet' => $query,
                ], 200);
            } else {
                return response([
                    'message' => 'Empty data',
                    'data' => [],
                ], 200);
            }
        } else {
            return response()->json([
                'status' => 'error',
                'message' => 'Something went wrong!',
            ], 401);
        }
    }

    public function updatePassword($driverId, array $request)
    {
        $user = Driver::where('driverId', $driverId)->first();
        if (!$user || !Hash::check($request['old_password'], $user->driver_password)) {
            return response([
                'message' => 'Bad credentials'
            ], 401);
        }
        $data = [
            'driver_password' => Hash::make($request['new_password']),
        ];

        $result = Driver::where('driverId', $driverId)->update($data);
        if ($result) {
            return response([
                'message' => 'Success',
            ], 201);
        }
    }
}
