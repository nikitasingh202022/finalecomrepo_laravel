<?php

namespace App\Http\Controllers;

use App\Repository\Interface\CommonInterface;
use App\Repository\Interface\DriverInterface;
use Illuminate\Http\Request;

class DriverController extends Controller
{
    public $driver;
    public $common;

    public function __construct(DriverInterface $driver, CommonInterface $common)
    {
        $this->common = $common;
        $this->driver = $driver;
    }


    public function updatePassword($driverId, Request $request)
    {
        $password = $request->all();
        $driverId = $request->driverId;
        // dd($password);
        return $this->driver->updatePassword($driverId, $password);
    }


    public function getEarningHistory(Request $request)
    {
        $data = $request->driverId;
        return $this->driver->getEarningHistory($data);
    }
    public function getDriverWallet(Request $request)
    {
        $data = $request->driverId;
        return $this->driver->getDriverWallet($data);
    }
}
