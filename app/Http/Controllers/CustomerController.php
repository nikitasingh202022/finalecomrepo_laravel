<?php

namespace App\Http\Controllers;

use App\Repository\Interface\CommonInterface;
use App\Repository\Interface\CustomerInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;

class CustomerController extends Controller
{
    public $customer;
    public $common;

    public function __construct(CustomerInterface $customer, CommonInterface $common)
    {
        $this->common = $common;
        $this->customer = $customer;
        $this->middleware('auth:api', ['except' => ['login','register','forgotPassword','getProductDetail','getProductListing','getBrandListing','getCategoryTypeListing','getCategoryListing','getSubCategoryListing','getReviewList']]);
    }

    public function register(Request $request){

        $request->validate([
            'fullName' => 'required|string',
            'gender' => 'required|string',
            'dob' => 'required',
            'email' => 'required|email|unique:tbl_customers,email',
            'password' => 'required|string|min:6',
            'mobile' => 'required|numeric|min:10|unique:tbl_customers,mobile',
            'address' => 'required|string',
            'cityId' => 'required',
            'stateId' => 'required',
            'countryId' => 'required',
            'countryCode' => 'required',
            'profilePicture' => 'required',
        ]);

        if($image = $request->file('profilePicture'))
        {
            $fileName = time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/customerProfiles', $fileName);
        }

        $data = [
            'fullName' => $request->input('fullName'),
            'gender' => $request->input('gender'),
            'dob' => date('Y-m-d', strtotime($request->input('dob'))),
            'email' => $request->input('email'),
            'password' => Hash::make($request->input('password')),
            'mobile' => $request->input('mobile'),
            'address' => $request->input('address'),
            'cityId' => $request->input('cityId'),
            'stateId' => $request->input('stateId'),
            'countryId' => $request->input('countryId'),
            'countryCode' => $request->input('countryCode'),
            'profilePicture' => $fileName,
        ];

        return $this->customer->customerRegistration($data);

    }

    public function login(Request $request)
    {
        $request->validate([
            'email' => 'sometimes|required|string|email',
            'mobile' => 'required_without:email|integer|string|numeric',
            'password' => 'required|string',
        ],[
            'mobile.required_without' => 'Please Enter Email or Mobile.',
        ]);

        $credentials = $request->input('email') ? $request->only('email', 'password') : $request->only('mobile', 'password');

        return $this->customer->customerLogin($credentials);

    }

    public function logout(Request $request)
    {
        return $this->customer->customerLogout();
    }

    public function getProfile($uuid)
    {
        return $this->customer->getCustomerProfile($uuid);
    }

    public function updateProfile($uuid, Request $request)
    {
        $fullName = $request->input('fullName')!="" ? $request->input('fullName') : "";
        $gender = $request->input('gender')!="" ? $request->input('gender') : "";
        $dob = $request->input('dob')!="" ? date('Y-m-d', strtotime($request->input('dob'))): "";
        $email = $request->input('email')!=""? $request->input('email') : "";
        $mobile = $request->input('mobile')!=""? $request->input('mobile') : "";
        $address = $request->input('address')!=""? $request->input('address') : "";
        $cityId = $request->input('cityId')!=""? $request->input('cityId') : "";
        $stateId = $request->input('stateId')!=""? $request->input('stateId') : "";
        $countryId = $request->input('countryId')!=""? $request->input('countryId') : "";
        $countryCode = $request->input('countryCode')!=""? $request->input('countryCode') : "";
        $image = $request->file('profilePicture');
        if(isset($image) && $image!="")
        {
            $fileName = "customerProfile" . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/customerProfiles', $fileName);
            $data['profilePicture'] = $fileName;
        }

        if(isset($fullName) && $fullName!=""){
            $data['fullName'] = $fullName;
        }
        if(isset($gender) && $gender!=""){
            $data['gender'] = $gender;
        }
        if(isset($dob) && $dob!=""){
            $data['dob'] = $dob;
        }
        if(isset($email) && $email!=""){
            $data['email'] = $email;
        }
        if(isset($mobile) && $mobile!=""){
            $data['mobile'] = $mobile;
        }
        if(isset($address) && $address!=""){
            $data['address'] = $address;
        }
        if(isset($cityId) && $cityId!=""){
            $data['cityId'] = $cityId;
        }
        if(isset($stateId) && $stateId!=""){
            $data['stateId'] = $stateId;
        }
        if(isset($countryId) && $countryId!=""){
            $data['countryId'] = $countryId;
        }
        if(isset($countryCode) && $countryCode!=""){
            $data['countryCode'] = $countryCode;
        }

        return $this->customer->updateCustomerProfile($uuid, $data);
    }

    public function addAddress($uuid, Request $request)
    {
        $validate = $request->validate([
            'addressType' => 'required|string',
            'address' => 'required|string',
            'cityId' => 'required|numeric',
            'stateId' => 'required|numeric',
            'countryId' => 'required|numeric',
            'pinCode' => 'required|numeric',
        ]);
        // $data = [
        //     'addressType' => $request->post('addressType'),
        //     'address' => $request->post('address'),
        //     'cityId' => $request->post('cityId'),
        //     'stateId' => $request->post('stateId'),
        //     'countryId' => $request->post('countryId'),
        //     'countryCode' => $request->post('countryCode'),
        //     'pinCode' => $request->post('pinCode'),
        // ];
        return $this->customer->addAddress($uuid,$validate);
    }

    public function getAddressList($uuid)
    {
        return $this->customer->getAddressList($uuid);
    }

    public function getProductListing(Request $request)
    {
        $filter = $request->all();
        return $this->customer->getProductListing($filter,$key='');
    }

    public function getProductDetail($uuid)
    {
        return $this->customer->getProductDetail($uuid);
    }

    public function getBrandListing()
    {
        return $this->customer->getBrandListing($key='');
    }

    public function getCategoryTypeListing()
    {
        return $this->customer->getCategoryTypeListing($key='');
    }

    public function getCategoryListing()
    {
        return $this->customer->getCategoryListing($key='');
    }

    public function getSubCategoryListing(Request $request)
    {
        $categoryId=$request->categoryId;
        return $this->customer->getSubCategoryListing($categoryId,$key='');
    }

    public function addToCart(Request $request)
    {
        $validation = $request->validate([
                        'productId' => 'required|numeric',
                        'cartQty' => 'required|numeric',
                        'size' => 'required',
                    ]);
        return $this->customer->addToCart( $validation);
    }

    public function getCartDetails()
    {
        return $this->customer->getCartDetails($key='');
    }

    public function removeFromCart($id)
    {
        return $this->customer->removeFromCart($id);
    }

    public function addToWishlist(Request $request)
    {

        $data = $request->validate([
            'productId' => 'required|numeric',
            'categoryType' => 'required|string',
            'category' => 'required|string',
        ]);
        return $this->customer->addToWishlist($data);
    }

    public function getWishlistData(Request $request)
    {
        $filter = $request->filter;
        return $this->customer->getWishlistData($filter,$key="");
    }

    public function removeFromWishlist($id)
    {
        return $this->customer->removeFromWishlist($id);
    }

    public function addReview(Request $request)
    {
        $data = $request->validate([
            'productId' => 'required|numeric' ,
            'rating' => 'required|numeric|digits_between:0,6' ,
            'review' => 'required|string' ,
            'reviewFile' => 'sometimes|array',
            'reviewFile.*' => 'image|max:5000|mimes:jpg,jpeg,png'
        ]);
        return $this->customer->addReview($data);
    }

    public function getReviewList($productId)
    {
        return $this->customer->getReviewList($productId,$key="");
    }

    public function getMyReviewList()
    {
        return $this->customer->getMyReviewList($key="");
    }

    public function getFaqListing()
    {
        return $this->customer->getFaqListing($key="");
    }

    public function getAllOrder()
    {
        return $this->customer->getAllOrder($key="");
    }

    public function getOrderDetail($uuid)
    {
        return $this->customer->getOrderDetail($uuid);

    }


































/*     public function resetPassword($uuid, Request $request)
    {
        $data= $request->all();
        return $this->customer->resetPassword($uuid, $data);
    }

    public function changePassword($uuid, Request $request)
    {
        $data= $request->all();
        return $this->customer->changePassword($uuid,$data);
    }

    public function forgotPassword(Request $request)
    {
        $data= $request->all();
        return $this->customer->forgotPassword($data);
    } */
}



