<?php

namespace App\Http\Controllers;

use App\Repository\Interface\AdminInterface;
use App\Repository\Interface\CommonInterface;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminController extends Controller
{
    public $admin;
    public $common;

    public function __construct(AdminInterface $admin, CommonInterface $common)
    {
        $this->common = $common;
        $this->admin = $admin;
        $this->middleware('auth:api', ['except' => ['adminRegister', 'adminLogin', 'getOrderDetail']]);
        Config::set('auth.providers.customers.model', \App\Models\Admin::class);
    }

    public function adminRegister(Request $request)
    {
        $request->validate([
            'adminName' => 'required|string',
            'dob' => 'required|date',
            'adminEmail' => 'required|email|unique:tbl_admins,adminEmail',
            'password' => 'required|string|min:6',
            'adminMobile' => 'required|numeric|min:10|unique:tbl_admins,adminMobile',
            'adminAddress' => 'required|string',
            'adminProfilePicture' => 'required',
        ]);

        if ($image = $request->file('adminProfilePicture')) {
            $fileName = 'adminProfile' . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/adminProfiles', $fileName);
        }

        $data = [
            'adminName' => $request->input('adminName'),
            'dob' => date('Y-m-d', strtotime($request->input('dob'))),
            'adminEmail' => $request->input('adminEmail'),
            'password' => Hash::make($request->input('password')),
            'adminMobile' => $request->input('adminMobile'),
            'adminAddress' => $request->input('adminAddress'),
            'adminRole' => 1,
            'adminProfilePicture' => $fileName,
        ];
        return $this->admin->adminRegister($data);
    }

    public function adminLogin(Request $request)
    {
        $request->validate([
            'adminEmail' => 'sometimes|required|string|email',
            'adminMobile' => 'required_without:adminEmail|integer|string|numeric',
            'password' => 'required|string',
        ], [
            'adminMobile.required_without' => 'Please Enter Email or Mobile.',
        ]);

        $credentials = $request->input('adminEmail') ? $request->only('adminEmail', 'password') : $request->only('adminMobile', 'password');
        // dd($credentials);
        return $this->admin->adminLogin($credentials);
    }

    public function adminLogout(Request $request)
    {
        return $this->admin->adminLogout();
    }

    public function getAdminProfile($uuid)
    {
        return $this->admin->getAdminProfile($uuid);
    }

    public function updateAdminProfile($uuid, Request $request)
    {
        $data = [];
        $adminName = $request->input('adminName') != "" ? $request->input('adminName') : "";
        $adminMobile = $request->input('adminMobile') != "" ? $request->input('adminMobile') : "";
        $dob = $request->input('dob') != "" ? date('Y-m-d', strtotime($request->input('dob'))) : "";
        $adminAddress = $request->input('adminAddress') != "" ? $request->input('adminAddress') : "";
        $image = $request->file('adminProfilePicture');
        if (isset($image) && $image != "") {
            $fileName = "adminProfile" . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/adminProfiles', $fileName);
            $data['adminProfilePicture'] = $fileName;
        }

        if (isset($adminName) && $adminName != "") {
            $data['adminName'] = $adminName;
        }
        if (isset($adminMobile) && $adminMobile != "") {
            $data['adminMobile'] = $adminMobile;
        }
        if (isset($dob) && $dob != "") {
            $data['dob'] = $dob;
        }
        if (isset($adminAddress) && $adminAddress != "") {
            $data['adminAddress'] = $adminAddress;
        }
        return $this->admin->updateAdminProfile($uuid, $data);
    }

    public function getCustomerListing()
    {
        return $this->admin->getCustomerListing($key = "");
    }

    public function getCustomerDetail($uuid)
    {
        return $this->admin->getCustomerDetail($uuid);
    }

    public function getAllProducts()
    {
        return $this->admin->getAllProducts($key = '');
    }

    public function getProductDetail($uuid)
    {
        return $this->admin->getProductDetail($uuid);
    }

    public function addProduct(Request $request)
    {
        $request->validate([
            'catTypeId' => 'required|numeric',
            'catId' => 'required|numeric',
            'subCatId' => 'required|numeric',
            'brandId' => 'required|numeric',
            'productName' => 'required|string',
            'productDesc' => 'required|string',
            'productQty' => 'required|numeric',
            'unit' => 'required|string',
            'productMrp' => 'required|decimal:2',
            'productMsp' => 'required|decimal:2',
            'productMfd' => 'required|date',
            'productExp' => 'required|date',
        ]);
        $fileArray = [];
        if ($images = $request->file('productImage')) {
            foreach ($images as $image) {
                $newFileName = 'productImage' . rand(1234, 9999) . '.' . $image->getClientOriginalExtension();
                $path = $image->move(public_path() . '/images/productImages', $newFileName);
                array_push($fileArray, $newFileName);
            }
        }

        $data = [
            'catTypeId' => $request->input('catTypeId'),
            'catId' => $request->input('catId'),
            'subCatId' => $request->input('subCatId'),
            'brandId' => $request->input('brandId'),
            'productName' => $request->input('productName'),
            'productDesc' => $request->input('productDesc'),
            'productQty' => $request->input('productQty'),
            'unit' => $request->input('unit'),
            'productMrp' => $request->input('productMrp'),
            'productMsp' => $request->input('productMsp'),
            'productMfd' => date('Y-m-d', strtotime($request->input('productMfd'))),
            'productExp' => date('Y-m-d', strtotime($request->input('productExp'))),
        ];
        return $this->admin->addProduct($data, $fileArray);
    }

    public function updateProduct($uuid, Request $request)
    {
        $request->validate([
            'catTypeId' => 'numeric',
            'catId' => 'numeric',
            'subCatId' => 'numeric',
            'brandId' => 'numeric',
            'productName' => 'string',
            'productDesc' => 'string',
            'productQty' => 'numeric',
            'unit' => 'string',
            'productMrp' => 'decimal:2',
            'productMsp' => 'decimal:2',
            'productMfd' => 'date',
            'productExp' => 'date',
        ]);
        $data = [];
        $fileArray = [];
        $catTypeId = $request->input('catTypeId') != "" ? $request->input('catTypeId') : "";
        $catId = $request->input('catId') != "" ? $request->input('catId') : "";
        $subCatId = $request->input('subCatId') != "" ? $request->input('subCatId') : "";
        $brandId = $request->input('brandId') != "" ? $request->input('brandId') : "";
        $productName = $request->input('productName') != "" ? $request->input('productName') : "";
        $productDesc = $request->input('productDesc') != "" ? $request->input('productDesc') : "";
        $productQty = $request->input('productQty') != "" ? $request->input('productQty') : "";
        $unit = $request->input('unit') != "" ? $request->input('unit') : "";
        $productMrp = $request->input('productMrp') != "" ? $request->input('productMrp') : "";
        $productMsp = $request->input('productMsp') != "" ? $request->input('productMsp') : "";
        $productMfd = $request->input('productMfd') != "" ? date('Y-m-d', strtotime($request->input('productMfd'))) : "";
        $productExp = $request->input('productExp') != "" ? date('Y-m-d', strtotime($request->input('productExp'))) : "";
        $image = $request->file('adminProfilePicture');
        if ($images = $request->file('productImage')) {
            foreach ($images as $image) {
                $newFileName = 'productImage' . rand(1234, 9999) . '.' . $image->getClientOriginalExtension();
                $path = $image->move(public_path() . '/images/productImages', $newFileName);
                array_push($fileArray, $newFileName);
            }
        }
        if (isset($catTypeId) && $catTypeId != "") {
            $data['catTypeId'] = $catTypeId;
        }
        if (isset($catId) && $catId != "") {
            $data['catId'] = $catId;
        }
        if (isset($subCatId) && $subCatId != "") {
            $data['subCatId'] = $subCatId;
        }
        if (isset($brandId) && $brandId != "") {
            $data['brandId'] = $brandId;
        }
        if (isset($productName) && $productName != "") {
            $data['productName'] = $productName;
        }
        if (isset($productDesc) && $productDesc != "") {
            $data['productDesc'] = $productDesc;
        }
        if (isset($productQty) && $productQty != "") {
            $data['productQty'] = $productQty;
        }
        if (isset($unit) && $unit != "") {
            $data['unit'] = $unit;
        }
        if (isset($productMrp) && $productMrp != "") {
            $data['productMrp'] = $productMrp;
        }
        if (isset($productMsp) && $productMsp != "") {
            $data['productMsp'] = $productMsp;
        }
        if (isset($productMfd) && $productMfd != "") {
            $data['productMfd'] = $productMfd;
        }
        if (isset($productExp) && $productExp != "") {
            $data['productExp'] = $productExp;
        }
        $data = [
            'catTypeId' => $request->input('catTypeId'),
            'catId' => $request->input('catId'),
            'subCatId' => $request->input('subCatId'),
            'brandId' => $request->input('brandId'),
            'productName' => $request->input('productName'),
            'productDesc' => $request->input('productDesc'),
            'productQty' => $request->input('productQty'),
            'unit' => $request->input('unit'),
            'productMrp' => $request->input('productMrp'),
            'productMsp' => $request->input('productMsp'),
            'productMfd' => date('Y-m-d', strtotime($request->input('productMfd'))),
            'productExp' => date('Y-m-d', strtotime($request->input('productExp'))),
        ];
        return $this->admin->updateProduct($uuid, $data, $fileArray);
    }

    public function deleteProduct($productId)
    {
        return $this->admin->deleteProduct($productId);
    }



    public function addBrand(Request $request)
    {
        $request->validate([
            'brandName' => 'required|string',
            'brandImg' => ['required'],
        ]);
        $data['brandName'] = $request->input('brandName');

        if ($image = $request->file('brandImg')) {
            $fileName = "brandImage" . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/brandImages', $fileName);
            $data['brandImg'] = $fileName;
        }
        return $this->admin->addBrand($data);
    }

    public function getBrandListing()
    {
        return $this->admin->getBrandListing($key = "");
    }
    public function deleteBrand($brandId)
    {
        return $this->admin->deleteBrand($brandId);
    }

    public function addCategory(Request $request)
    {
        $request->validate([
            'catTypeId' => 'required',
            'catName' => 'required|string',
            'catImg' => ['required'],
        ]);
        $data['catTypeId'] = $request->input('catTypeId');
        $data['catName'] = $request->input('catName');
        if ($image = $request->file('catImg')) {
            $fileName = "catImg" . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/catImgs', $fileName);
            $data['catImg'] = $fileName;
        }
        return $this->admin->addCategory($data);
    }
    public function getCategoryListing()
    {
        return $this->admin->getCategoryListing($key = "");
    }
    public function deleteCategory($catId)
    {
        return $this->admin->deleteCategory($catId);
    }

    public function addCategoriesType(Request $request)
    {
        $request->validate([
            'catTypeName' => 'required|string',
            'catTypeImg' => ['required'],
        ]);
        $data['catTypeName'] = $request->input('catTypeName');
        if ($image = $request->file('catTypeImg')) {
            $fileName = "catTypeImg" . time() . '.' . $image->getClientOriginalExtension();
            $path = $image->move(public_path() . '/images/catTypeImgs', $fileName);
            $data['catTypeImg'] = $fileName;
        }
        return $this->admin->addCategoriesType($data);
    }


    public function getCompletedOrders(Request $request)
    {
        $data = $request->orderId;
        return $this->admin->getCompletedOrders($data);
    }

    public function getCancelledOrders(Request $request)
    {
        $data = $request->orderId;
        return $this->admin->getCancelledOrders($data);
    }

    public function getOrdersListing()
    {
        return $this->admin->getOrdersListing($key = "");
    }
    public function getAllOrderDetail($orderId)
    {
        return $this->admin->getOrderDetail($orderId);
    }
   


}
