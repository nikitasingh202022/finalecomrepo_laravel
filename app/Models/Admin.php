<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
// use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Admin extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = "tbl_admins";

    protected $fillable = [
        'uuid',
        'adminRole',
        'adminName',
        'adminEmail',
        'adminPassword',
        'adminMobile',
        'dob',
        'adminProfilePicture',
        'adminAddress',
    ];

    protected $primaryKey = 'adminId';
    public $timestamps = false;

    protected $hidden = [
        'adminPassword',
        'remember_token',
    ];

    protected $casts = [
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }
}
