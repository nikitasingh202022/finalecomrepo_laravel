<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Driver_job extends Model
{
    use HasFactory;
    protected $table = "tbl_driver_jobs";
}
