<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Address extends Model
{
    use HasFactory;

    protected $table = "tbl_addresses";

    protected $fillable = [
        'addressType',
        'address',
        'customerId',
        'cityId',
        'stateId',
        'countryId',
        'pinCode',
    ];

    protected $primaryKey = 'addressId';
    public $timestamps = false;
}
