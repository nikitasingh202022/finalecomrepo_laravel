<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    protected $table = "tbl_products";

    protected $fillable = [
        'productId',
        'uuid',
        'catTypeId',
        'catId',
        'subCatId',
        'brandId',
        'productName',
        'productImageId',
        'productDesc',
        'productQty',
        'unit',
        'productMrp',
        'productMsp',
        'productMfd',
        'productExp',
    ];

    protected $primaryKey = 'productId';
    public $timestamps = false;
}
