<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;
    protected $table = "tbl_orders";

    protected $fillable = [
        'orderId',
        'uuid',
        'orderNo',
        'orderCode',
        'customerId',
        'couponCode',
        'orderAddress',
        'totalAmt',
        'discountedAmt',
        'paymentMode',
        'transId',
        'paymentStatus',
        "orderStatus"
    ];

    protected $primaryKey = 'productId';
    public $timestamps = false;
}
