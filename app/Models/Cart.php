<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Cart extends Model
{
    use HasFactory;
    protected $table = "tbl_carts";

    protected $fillable = [
        'cartId',	'uuid',	'customerId',	'productId',	'cartQty',	'size'
    ];

    protected $primaryKey = 'cartId';
    public $timestamps = false;
}
