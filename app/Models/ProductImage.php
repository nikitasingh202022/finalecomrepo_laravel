<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductImage extends Model
{
    use HasFactory;
    protected $table = "tbl_product_images";

    protected $fillable = [
        'productImageId',
        'productImage',
        'productId',
    ];

    protected $primaryKey = 'productImageId';
    public $timestamps = false;
}
