<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
// use PHPOpenSourceSaver\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Customer extends Authenticatable implements JWTSubject
{
    use HasApiTokens, HasFactory, Notifiable;

    protected $table = "tbl_customers";

    protected $fillable = [
        'contInfoId','fullName','gender','dob','email','password','isEmailVerified','mobile','isMobileVerified','otp','profilePicture','address','cityId','stateId','countryId','countryCode','deviceType','walletAmount','isActive','isDeleted','createdAt','updatedAt','deletedAt',
    ];

    protected $primaryKey = 'customerId';

    protected $hidden = [
        'password',
        'remember_token',
    ];

    protected $casts = [
    ];

    public $timestamps = false;

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }


}
