<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Wishlist extends Model
{
    use HasFactory;
    protected $table = "tbl_wishlist";

    protected $fillable = [
        'wishlistId',	'uuid',	'customerId',	'productId',	'catTypeId',	'catId',
    ];

    protected $primaryKey = 'wishlistId';
    public $timestamps = false;
}
