<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Review extends Model
{
    use HasFactory;
    protected $table = "tbl_reviews";

    protected $fillable = [
        'reviewId',	'uuid',	'productId',	'customerId',	'rating',	'review',	'reviewFile'
    ];

    protected $primaryKey = 'reviewId';
    public $timestamps = false;
}
