<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    use HasFactory;
    protected $table = "tbl_brands";

    protected $fillable = [
        'brandId','uuid','brandName','brandImg'
    ];

    protected $primaryKey = 'brandId';
    public $timestamps = false;
}
